#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <pthread.h>
#include <semaphore.h>
#include <iostream>
#include "common.h"

using namespace std;
#endif /* __PROGTEST__ */

typedef uint8_t byte;
typedef uint32_t uint32;
typedef unsigned int uint;

pthread_mutex_t programExitMutex;
pthread_mutex_t processIdMutex;
pthread_mutex_t processCountMutex;
unsigned long long processId = 0;
unsigned long long processCount = 0;

void processCountInc() {
	pthread_mutex_lock(&processCountMutex);
	processCount += 1;
	pthread_mutex_unlock(&processCountMutex);
}

unsigned long long getProcessId() {
	pthread_mutex_lock(&processIdMutex);
	unsigned long long id = processId;
	processId += 1;
	pthread_mutex_unlock(&processIdMutex);

	return id;
}

uint roundedDivision(uint a, uint b) {
	return (a / b) + (a % b > 0 ? 1 : 0);
}

struct ProccessArgs {
	CCPU * cpu;

	void (* entryPoint)(CCPU *, void *);

	void * processArg;
};

void * startProcess(void * arg) {
	ProccessArgs * args = (ProccessArgs *) arg;
	args->entryPoint(args->cpu, args->processArg);

	delete args->cpu;
	delete args;

	return nullptr;
}

class PageIterator {
private:
	const uint32 ACCESS_MASK = CCPU::BIT_USER + CCPU::BIT_WRITE + CCPU::BIT_PRESENT;
	byte * memStart;
	uint32 rootPage;
	uint32 totalPages;
	uint32 currentPage;
	bool valid = true;
public:

	PageIterator(byte * memStart, uint32 rootPage, uint32 totalPages, uint32 currentPage)
		: memStart(memStart), rootPage(rootPage), totalPages(totalPages), currentPage(currentPage) {
	}

	/**
	 * Calculates page address from current index and returns level 1 page.
	 *
	 * @return Pointer to level 1 page.
	 */
	uint32 * level1() const {
		uint32 indexL1 = this->currentPage / CCPU::PAGE_DIR_ENTRIES;
		uint32 * level1 = (uint32 *) (this->memStart + (this->rootPage & CCPU::ADDR_MASK));

		return level1 + indexL1;
	}

	/**
	 * Calculates page address from current index and returns level 2 page.
	 *
	 * @return Pointer to level 2 page.
	 */
	uint32 * level2() const {
		uint32 * level1 = this->level1();

		if ((* level1 & ACCESS_MASK) != ACCESS_MASK) {
			throw exception();
		}

		uint32 indexL2 = this->currentPage % CCPU::PAGE_DIR_ENTRIES;
		uint32 * level2 = (uint32 *) (this->memStart + (* level1 & CCPU::ADDR_MASK));

		return level2 + indexL2;
	}

	bool isAccessible() const {
		return (* this->level1() & ACCESS_MASK) == ACCESS_MASK;
	}

	/**
	 * Returns TRUE if last increment or decrement operation failed.
	 */
	bool operator!() const {
		return !((bool) * this);
	}

	/**
	 * Returns TRUE if last increment or decrement operation succeeded.
	 */
	operator bool() const {
		return this->valid;
	}

	/**
	 * Returns TRUE if given comparator references the same page, FALSE otherwise.
	 */
	bool operator==(PageIterator & other) const {
		return this->currentPage == other.currentPage;
	}

	/**
	 * Returns TRUE if given comparator does not reference the same page, FALSE otherwise.
	 */
	bool operator!=(PageIterator & other) const {
		return !(* this == other);
	}

	/**
	 * Increments iterator so it references next page in the virtual memory of the process.
	 *
	 * @return This instance.
	 */
	PageIterator & operator++() {
		if ((this->valid = (this->currentPage < this->totalPages))) {
			++this->currentPage;
		}

		return * this;
	}

	/**
	 * Decrements iterator so it references previous page in the virtual memory of the process.
	 *
	 * @return This instance.
	 */
	PageIterator & operator--() {
		if ((this->valid = (this->currentPage > 0))) {
			--this->currentPage;
		}

		return * this;
	}

	/**
	 * Increments iterator so it references next page in the virtual memory of the process.
	 *
	 * @return Copy of this instance before the increment.
	 */
	PageIterator operator++(int a) {
		PageIterator copy(* this);
		++* this;

		return copy;
	}

	/**
	 * Decrements iterator so it references previous page in the virtual memory of the process.
	 *
	 * @return Copy of this instance before the decrement.
	 */
	PageIterator operator--(int a) {
		PageIterator copy(* this);
		--* this;

		return copy;
	}
};

class MemoryManager {
private:

	/** Standard mask representing accesible page. */
	const uint32 ACCESS_MASK = CCPU::BIT_USER + CCPU::BIT_WRITE + CCPU::BIT_PRESENT;

	/** Special ownership ID for free frame. */
	const byte FREE_PAGE = (byte) -1;

	/** Special ownership ID for frame allocated by memory manager, not process. */
	const byte MEMORY_MANAGER = (byte) -2;

	/** Pointer to first byte of the allocated memory. */
	byte * const memStart;

	/** Size of the memory in pages. */
	const uint32 totalPages;

	/** Count of pages free to allocate. */
	uint32 freePages;

	/** Mutex for memStart. */
	pthread_mutex_t mutex;

public:

	explicit MemoryManager(byte * memStart, uint32 totalPages) : memStart(memStart), totalPages(totalPages) {
		pthread_mutex_init(& this->mutex, NULL);

		// Allocate few pages for memory manager itself
		uint32 memoryManagerPages = roundedDivision(this->totalPages, CCPU::PAGE_SIZE);

		for (uint32 i = 0; i < this->totalPages; ++i) {
			this->memStart[i] = (i < memoryManagerPages ? MEMORY_MANAGER : FREE_PAGE);
		}

		this->freePages = totalPages - memoryManagerPages;
	}

	~MemoryManager() {
		pthread_mutex_destroy(& this->mutex);
	}

	/**
	 * Gets pointer to the first byte of allocated memory.
	 *
	 * @return Pointer to the first byte.
	 */
	const byte * const getMemStart() const {
		return this->memStart;
	}

	/**
	 * Creates all L1 pages for given process and fills them as not present.
	 *
	 * @param pid Process ID.
	 * @return Root page.
	 */
	uint32 createRootPage(pthread_t pid) {
		this->lock();
		uint32 count = roundedDivision(this->totalPages, CCPU::PAGE_SIZE);
		uint32 index = this->allocateFreePages((byte) pid, count);
		uint32 root = 0;

		if (index > 0) {
			root = (index << CCPU::OFFSET_BITS) | CCPU::BIT_USER | CCPU::BIT_WRITE | CCPU::BIT_PRESENT;
			uint32 * level1 = (uint32 *) (this->memStart + (root & CCPU::ADDR_MASK));

			for (uint32 i = 0; i < (count * CCPU::PAGE_DIR_ENTRIES); ++i) {
				level1[i] = ~CCPU::BIT_PRESENT;
			}
		}

		this->unlock();
		return root;
	}

	/**
	 * Resizes virtual memory by given parameters.
	 *
	 * @param pid Process ID of which the memory is being resized.
	 * @param rootPage Process root page.
	 * @param currentPageCount Current count of pages.
	 * @param newPageCount New count of pages.
	 * @return Root page or 0 if unsuccessful.
	 */
	uint32 resizeVirtualMemory(pthread_t pid, uint32 rootPage, uint32 currentPageCount, uint32 newPageCount) {
		this->lock();

		PageIterator current(this->memStart, rootPage, this->totalPages, currentPageCount);
		PageIterator last(this->memStart, rootPage, this->totalPages, newPageCount);

		if (newPageCount > currentPageCount && this->freePages < (newPageCount - currentPageCount)) {
			rootPage = 0;
			goto leave;
		}

		if (newPageCount > currentPageCount) {
			uint32 page;
			uint32 newPageCountAligned = newPageCount;

			if (newPageCount % CCPU::PAGE_DIR_ENTRIES != 0) {
				newPageCountAligned += (CCPU::PAGE_DIR_ENTRIES - newPageCount % CCPU::PAGE_DIR_ENTRIES);
			}

			PageIterator lastAligned(this->memStart, rootPage, this->totalPages, newPageCountAligned);

			// Fill in L1 and L2 pages, allocate pages
			for (; current != last; ++current) {
				if (!current.isAccessible()) {
					// Allocate L2 page
					if (!(page = this->allocateFreePages((byte) pid))) {
						rootPage = 0;
						goto leave;
					}

					* current.level1() = (page << CCPU::OFFSET_BITS) + ACCESS_MASK;
				}

				// Allocate frame page
				if (!(page = this->allocateFreePages((byte) pid))) {
					rootPage = 0;
					goto leave;
				}

				* current.level2() = (page << CCPU::OFFSET_BITS) + ACCESS_MASK;
			}

			// Fill in rest of the L2 table with inaccessible pages.
			for (; current != lastAligned; ++current) {
				if (!current) {
					goto leave;
				}

				* current.level2() = ~CCPU::BIT_PRESENT;
			}
		}
		else if (newPageCount < currentPageCount) {
			uint32 * prevLevel1 = current.level1();
			current--;

			do {
				uint32 * level1 = current.level1();
				uint32 * level2 = current.level2();

				if (level1 != prevLevel1) {
					this->releasePage(* prevLevel1 >> CCPU::OFFSET_BITS);
					* prevLevel1 = ~CCPU::BIT_PRESENT;

					prevLevel1 = level1;
				}

				this->releasePage(* level2 >> CCPU::OFFSET_BITS);
				* level2 = ~CCPU::BIT_PRESENT;
			}
			while (current-- != last);
		}

leave:
		this->unlock();
		return rootPage;
	}

	/**
	 * Copies contents of virtual memory of one process to another's virtual memory space.
	 *
	 * @param spid
	 * @param dpid
	 * @param sourceRoot
	 * @param destRoot
	 * @param count
	 */
	bool copyVirtualMemory(pthread_t spid, pthread_t dpid, uint32 sourceRoot, uint32 destRoot, uint32 count) {
		this->lock();

		PageIterator src(this->memStart, sourceRoot, count, 0);
		PageIterator dst(this->memStart, destRoot, count, 0);
		PageIterator dstEnd(this->memStart, destRoot, count, count);
		uint32 page;
		bool status = true;

		if (this->freePages < count) {
			status = false;
			goto leave;
		}

		while (src != dstEnd) {
			if (!src.isAccessible()) {
				* dst.level1() = ~CCPU::BIT_PRESENT;
			}
			else {
				if (!dst.isAccessible()) {
					// Allocate L2 page
					if (!(page = this->allocateFreePages((byte) dpid))) {
						status = false;
						goto leave;
					}

					(* dst.level1()) = (page << CCPU::OFFSET_BITS) + ACCESS_MASK;
				}
				if (!dst.isAccessible()) {
					throw exception();
				}

				// Allocate frame page
				if (!(page = this->allocateFreePages((byte) dpid))) {
					status = false;
					goto leave;
				}

				(* dst.level2()) = (page << CCPU::OFFSET_BITS) + ACCESS_MASK;

				byte * memorySrc = (memStart + (* src.level2() & CCPU::ADDR_MASK));
				byte * memoryDst = (memStart + (* dst.level2() & CCPU::ADDR_MASK));

				for (size_t i = 0; i < CCPU::PAGE_SIZE; ++i) {
					memoryDst[i] = memorySrc[i];
				}
			}

			++src;
			++dst;
		}

leave:
		this->unlock();
		return status;
	}

	/**
	 * Removes all page references by given process ID.
	 *
	 * @param pid Process ID to dereference the pages of.
	 */
	void releasePages(pthread_t pid) {
		this->lock();

		for (uint32 i = 0; i < this->totalPages; ++i) {
			if (this->memStart[i] == pid) {
				this->freePages += 1;
				this->memStart[i] = FREE_PAGE;
			}
		}

		this->unlock();
	}

	/**
	 * Dumps memory ownership array, for debugging.
	 *
	 * @param out Stream to write to.
	 */
	void dump(ostream & out) {
		this->lock();

		for (size_t i = 0; i < this->totalPages; ++i) {
			out << i << ": " << (uint) this->memStart[i] << endl;
		}

		this->unlock();
	}

private:

	/**
	 * Dereferences page by index.
	 *
	 * @param index Index of the page to dereference.
	 */
	void releasePage(uint32 index) {
		this->memStart[index] = FREE_PAGE;
		this->freePages += 1;
	}

	/**
	 * Allocates free page or a sequence of free pages.
	 *
	 * @param pid Process ID that will reference the pages.
	 * @param sequence Sequence length.
	 * @return Index of the first page or zero if such sequence could not be found.
	 */
	uint32 allocateFreePages(byte pid, uint32 sequence = 1) {
		uint32 index = this->findFreePages(sequence);

		if (!index) return 0;

		for (size_t i = 0; i < sequence; ++i) {
			this->memStart[index + i] = pid;
			this->freePages -= 1;
		}

		return index;
	}

	/**
	 * Finds free page or a sequence of free pages.
	 *
	 * @param sequence Sequence length.
	 * @return Index of the first page or zero if such sequence could not be found.
	 */
	uint32 findFreePages(uint32 sequence = 1) const {
		for (uint32 i = 0; i < this->totalPages; ++i) {
			if (this->memStart[i] != FREE_PAGE) continue;

			bool ok = true;

			// Check following bytes up to "sequence" count
			for (size_t j = 0; j < sequence; ++j) {
				if (this->memStart[i] != FREE_PAGE) {
					ok = false;
					break;
				}
			}

			if (ok) return i;
		}

		return 0;
	}

	void lock() {
		pthread_mutex_lock(& this->mutex);
	}

	void unlock() {
		pthread_mutex_unlock(& this->mutex);
	}
};

class NotEnoughMemoryException : public exception {
};

class FrederickProcessor : public CCPU {
private:

	/** MMU, shared with all Fredericks. */
	MemoryManager & mmu;

	/** Count of pages allocated by this process. */
	uint32 pageCount = 0;

	/** Process ID of this Frederick. */
	pthread_t pid;

	/** Thread ID of this Frederick. */
	pthread_t tid;

public:

	FrederickProcessor(MemoryManager & mmu, pthread_t pid)
		: CCPU((byte *) mmu.getMemStart(), mmu.createRootPage(pid)), mmu(mmu), pid(pid) {
		if (this->m_PageTableRoot == 0) {
			throw NotEnoughMemoryException();
		}
		processCountInc();
	}

	FrederickProcessor(const FrederickProcessor & o, pthread_t pid)
		: CCPU((byte *) o.mmu.getMemStart(), o.mmu.createRootPage(pid)), mmu(o.mmu), pageCount(o.pageCount), pid(pid) {
		if (this->m_PageTableRoot == 0) {
			throw NotEnoughMemoryException();
		}
		if (!this->mmu.copyVirtualMemory(o.pid, this->pid, o.m_PageTableRoot, this->m_PageTableRoot, this->pageCount)) {
			throw NotEnoughMemoryException();
		}

		processCountInc();
	}

	~FrederickProcessor() {
		this->mmu.releasePages(this->pid);

		pthread_mutex_lock(&processCountMutex);
		processCount -= 1;
		if (processCount == 0) {
			pthread_mutex_unlock(&programExitMutex);
		}
		pthread_mutex_unlock(&processCountMutex);
	}

	uint32 GetMemLimit() const override {
		return this->pageCount;
	}

	bool SetMemLimit(uint32 pageCount) override {
		uint32 root = this->mmu.resizeVirtualMemory(this->pid, this->m_PageTableRoot, this->pageCount, pageCount);

		if (!root) return false;

		this->m_PageTableRoot = root;
		this->pageCount = pageCount;

		return true;
	}

	bool NewProcess(void * processArg, void (* entryPoint)(CCPU *, void *), bool copyMem) override {
		FrederickProcessor * subCpu = nullptr;

		try {
			if (copyMem) {
				subCpu = new FrederickProcessor(* this, getProcessId());
			}
			else {
				subCpu = new FrederickProcessor(this->mmu, getProcessId());
			}
		}
		catch (const NotEnoughMemoryException & e) {
			return false;
		}

		ProccessArgs * args = new ProccessArgs();
		args->cpu = subCpu;
		args->processArg = processArg;
		args->entryPoint = entryPoint;

		pthread_create(& subCpu->tid, nullptr, startProcess, args);

		return true;
	}

protected:

	/*
	 if copy-on-write is implemented:

	 virtual bool pageFaultHandler(uint32_t address, bool write);
	 */
};

void MemMgr(void * mem, uint32 totalPages, void * processArg, void (* mainProcess)(CCPU *, void *)) {
	byte * memory = (byte *) mem;
	pthread_mutex_init(&processIdMutex, NULL);
	pthread_mutex_init(&processCountMutex, NULL);
	pthread_mutex_init(&programExitMutex, NULL);
	pthread_mutex_lock(&programExitMutex);

	MemoryManager mmu(memory, totalPages);
	FrederickProcessor * cpu = new FrederickProcessor(mmu, getProcessId());
	mainProcess(cpu, processArg);
	delete cpu;

	pthread_mutex_lock(&programExitMutex);
	pthread_mutex_lock(&processCountMutex);
	if (processCount != 0) throw exception();
	pthread_mutex_unlock(&processCountMutex);
	pthread_mutex_unlock(&programExitMutex);

	pthread_mutex_destroy(&processIdMutex);
	pthread_mutex_destroy(&processCountMutex);
	pthread_mutex_destroy(&programExitMutex);
}
